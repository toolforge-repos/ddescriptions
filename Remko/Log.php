<?php

/*
 * Copyright (C) 2018 Remko
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Remko;

use Exception;
use PHPMailer;

/**
 * Helper class for logging functions
 *
 * @author Remko
 *
 */
class Log {

    /**
     * Log result of ytcleaner
     *
     * @param string $text Line to be added to the logfile
     * @return void
     */
    function logytc($text) {
	try {
	    $file = fopen('ytcleaner.tsv', 'a+');
	    fwrite($file, $text);
	} catch (Exception $e) {
	    trigger_error($e->getMessage());
	}
    }

    /**
     * Function that sends a mail to the admin about Snaks that aren't mainsnaks. These need manual cleaning.
     *
     * @param array $nonMainSnakMail Array containing data for non mainsnaks
     * @return void
     */
    function logNonMainSnakMail($nonMainSnakMail) {
	$mail = new PHPMailer;
	$mail->SMTPDebug = 2;
	$mail->setFrom("mbch331.wikipedia@gmail.com", 'YouTube Channel ID cleaner for Wikidata');
	$mail->addAddress('mbch331.wikipedia@gmail.com', 'Mbch331');
	$mail->addReplyTo('mbch331.wikipedia@gmail.com', 'Mbch331');
	$mail->isHTML(true);
	$mail->CharSet = 'utf-8';
	$mail->Subject = "Non mainsnaks using P2397";
	$bodytable = "<table>\r\n<thead>\r\n<tr>\r\n<th>Item</th><th>Type</th><th>Value</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n";
	foreach ($nonMainSnakMail as $value) {
	    $bodytable .= "<tr>\r\n<td><a href=\"http://www.wikidata.org/wiki/Special:Entity/" . $value['item'] . "\">". $value['item'] ."</a></td><td>" . $value['type'] . "</td><td>" . $value['value'] . "</td>\r\n</tr>\r\n";
	}
	$bodytable .= "</tbody>\r\n</table>";
	$mail->Body = '<!--' . count($nonMainSnakMail) . '-->' . $bodytable;
	if (!$mail->send()) {
	    echo 'Message could not be sent.';
	    echo 'Mailer Error: ' . $mail->ErrorInfo;
	} else {
	    echo 'Message has been sent';
	}
    }

}
