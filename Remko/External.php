<?php

/*
 * Copyright (C) 2018 Remko
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Remko;

/**
 * functions that use external connections
 *
 * @author Remko
 */
class External {

    /**
     * Fetch a URL using CURL and return the results
     *
     * @param string $url URL to be opened
     * @param bool $trans Return de transferred data
     * @param int $post GET (0) or POST (1)
     * @param bool $close Does the connection needs to be closed?
     * @param string $postdata
     * @param string $error Does curl need to fail when an error gets returned
     * @param string $follow Does curl need to follow redirects?
     * @return string The transferred data if requested
     */
    function invokeURL($url, $trans = FALSE, $post = 0, $close = TRUE, $postdata = "", $error = false, $follow = false) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, $trans);
	curl_setopt($ch, CURLOPT_POST, $post);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mbchbot/0.1');
	curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookiefile.tmp');
	curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookiefile.tmp');
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_FAILONERROR, $error);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $follow);
	curl_getinfo($ch);
	if ($post == 1) {
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
	}
	$data = curl_exec($ch);
	if (isset($data) && gettype($data) == "string") {
	    return $data;
	} else {
	    if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 404) {
		return "";
	    } else {
		if (curl_errno($ch) == 3) {
		    trigger_error(curl_error($ch) . ": $url");
		}
		trigger_error(curl_error($ch));
	    }
	}
	if ($close) {
	    curl_close($ch);
	}
    }

    /**
     * Get YouTube data using Google YouTube Api
     *
     * @param string $userid
     * @param string $key
     * @return string
     */
    function checkYouTube($userid, &$key) {
	$url = "https://www.googleapis.com/youtube/v3/channels?part=id&forUsername=$userid&fields=items%2Fid&key=$key";
	$jsonres = $this->invokeURL($url, TRUE);
	$jsondata = (json_decode($jsonres, true));
	if (isset($jsondata['items'][0]['id'])) {
	    $returntext = $jsondata['items'][0]['id'];
	} else {
	    $returntext = "";
	}
	return $returntext;
    }

    /**
     *
     * @param atring $query The query to be executed
     * @return string Return the fetched data as a json
     */
    function checkWikidata($query = '') {
	$qry = urlencode($query);
	return $this->invokeURL("https://query.wikidata.org/bigdata/namespace/wdq/sparql?query=$qry&format=json", TRUE);
    }

    /**
     * Fetch data from Wikidata and return it as an array
     *
     * @param string $query The query to be executed
     * @return array $data Part of the decoded json as an array
     */
    function fetchData($query = '') {
	$dataWikidata = json_decode($this->checkWikidata($query), true);
	$data = $dataWikidata['results']['bindings'];
	return $data;
    }

}
