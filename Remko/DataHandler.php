<?php

/*
 * Copyright (C) 2018 Remko
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Remko;

use DataValues\Deserializers\DataValueDeserializer;
use DataValues\Serializers\DataValueSerializer;
use DataValues\StringValue;
use DOMDocument;
use Mediawiki\Api\ApiUser;
use Mediawiki\Api\MediawikiApi;
use Mediawiki\Api\UsageException as MwUsageException;
use Mediawiki\DataModel\EditInfo;
use Mediawiki\DataModel\Revision;
use Remko\Log;
use Wikibase\Api\WikibaseFactory;
use Wikibase\DataModel\Entity\PropertyId;
use Wikibase\DataModel\Snak\PropertyValueSnak;
use Wikibase\DataModel\Statement\Statement;

/**
 * Description of DataHandler
 *
 * @author Remko
 */
class DataHandler {

    protected $channelid;
    protected $services;
    private $editinfo;

    /**
     * Parse the YouTube data and try to get a channel id out of it
     *
     * @param string $ytid
     * @param External External
     * @return string channel id if found or else empty string
     */
    function parseytdata($ytid = "", $external) {
	$ytmatch = preg_match("/UC[-_A-Za-z0-9]{21}[AQgw]/", $ytid, $ytchannel);
	if ($ytmatch != 0) { //Channel id already present in link
	    $this->channelid = $ytchannel[0];
	} else {
	    $linkCheck = preg_match("/http(s)?\:\/\/(www\.)?youtube\.com\/(channel\/|user\/|c\/)?/i", $ytid);
	    if ($linkCheck == 0) {
		$ytid = 'http://www.youtube.com/' . $ytid; //Turn it into an URL
	    }
	    $ytpage = $external->invokeURL($ytid, TRUE, 0, TRUE, "", TRUE, TRUE);
	    $this->channelid = '""';
	    if ($ytpage != '') {
		$this->getChannelIdFromMetadata($ytpage);
	    } else {
		$this->channelid = '';
	    }
	    libxml_use_internal_errors(false); //Back to normal state
	}
	return $this->channelid;
    }

    /**
     * Parse the returned YouTube page and set the channel id from the metadata
     *
     * @param string $ytpage
     * @return void
     */
    protected function getChannelIdFromMetadata($ytpage) {
	$doc = new DOMDocument();
	libxml_use_internal_errors(true); //Otherwise loadHTML will fail on incorrect markup
	$doc->loadHTML($ytpage);
	$metas = $doc->getElementsByTagName('meta');
	if (gettype($metas) !== 'object') {
	    $this->channelid = '';
	} else {
	    foreach ($metas as $meta) {
		if ($meta->getAttribute('itemprop') == 'channelId') {
		    $this->channelid = $meta->getAttribute('content');
		}
	    }
	}
	libxml_use_internal_errors(false); //Back to normal state
    }

    /**
     * Get the QID from the Entity url
     *
     * @param string $entity Full entity URL
     * @return string Parsed QID from the entity URL
     */
    function getQidFromEntity($entity) {
	return str_replace('http://www.wikidata.org/entity/', '', $entity);
    }

    /**
     *
     * @global string $wduser Username for Wikidata api
     * @global string $wdpass Password for Wikidata api
     * @param EditInfo $editinfo
     * @param string $qid
     * @param string $guid
     * @param string $channelid
     * @param string $origin
     * @param string $type
     * @return MwUsageExpception|NULL Error if any
     */
    function updateWikidata($editinfo, $qid, $guid, $channelid, $origin, $type = 'update') {
	global $wduser, $wdpass;
	require_once( __DIR__ . '/../includes/accountdata.inc');
	$dataValueClasses = ['unknown' => 'DataValues\UnknownValue', 'string' => 'DataValues\StringValue'];
	$this->editinfo = $editinfo;
	$api = new MediawikiApi("https://www.wikidata.org/w/api.php");
	$api->login(new ApiUser($wduser, $wdpass));
	$this->services = new WikibaseFactory(
		$api, new DataValueDeserializer($dataValueClasses), new DataValueSerializer()
	);
	$revision = $this->services->newRevisionGetter()->getFromId($qid);
	$item = $revision->getContent()->getData();
	$statementList = $item->getStatements();
	$statement = $statementList->getByPropertyId(PropertyId::newFromNumber(2397));
	$mainStatement = $statement->getFirstStatementWithGuid($guid);
	$type == 'update' ? $this->updateStatement($mainStatement, $channelid, $revision, $origin, $guid, $qid) : $this->removeStatement($revision, $guid, $origin, $channelid);
	if (gettype($type) == 'object') {
	    return $type;
	}
    }

    /**
     *
     * @param Statement $mainStatement
     * @param string $channelid
     * @param Revision $revision
     * @param string $origin
     * @param string $guid
     * @param string $qid
     * @return MwUsageExpception|NULL Error if any
     */
    protected function updateStatement($mainStatement, $channelid, $revision, $origin, $guid, $qid) {
	$mainStatement->setMainSnak(new PropertyValueSnak(PropertyId::newFromNumber(2397), new StringValue($channelid)));
	return $this->saveUpdatedSnak($revision, $qid, $origin, $channelid, $guid);
    }

    /**
     *
     * @param Revision $revision
     * @param string $guid
     * @param string $origin
     * @param string $channelid
     * @return MwUsageExpception|NULL Error if any
     */
    protected function removeStatement($revision, $guid, $origin, $channelid) {
	try {
	    $removedStatement = $this->services->newStatementRemover()->remove($guid, $this->editinfo);
	} catch (MwUsageException $e) {
	    if ($e->getMessage() == '⧼abusefilter-warning-68⧽') {
		$removedStatement = $this->services->newStatementRemover()->remove($guid, $this->editinfo);
	    } else {
		$qid = preg_replace("/\-([a-zA-Z0-9-])+/i", "", $guid);
		$log = new Log();
		$log->logytc(date("Y-m-d H:i T") . "\t$qid\t$origin\t$channelid\t" . $e->getMessage());
	    }
	}
	return $this->saveUpdatedSnak($revision, $qid, $origin, $channelid, $guid);
    }

    /**
     *
     * @param Revision $revision
     * @param string $qid
     * @param string $origin
     * @param string $channelid
     * @param string $guid
     * @return MwUsageExpception|NULL Error if any
     */
    protected function saveUpdatedSnak($revision, $qid, $origin, $channelid, $guid) {
	try {
	    $this->services->newRevisionSaver()->save($revision, $this->editinfo);
	    return;
	} catch (MwUsageException $e) {
	    $qid = preg_replace("/\-([a-zA-Z0-9-])+/i", "", $guid);
	    $log = new Log();
	    $log->logytc(date("Y-m-d H:i T") . "\t$qid\t$origin\t$channelid\t" . $e->getMessage());
	    return $e;
	}
    }

    /**
     * 
     * @param $array $items Items without description
     * @return array Array containing the personal information stored in Wikidata
     */
    function getPersonData($items) {
        $queryhandler = new External();
        $this->querytxt = fread(fopen(__DIR__.'/../includes/persondata.sql','r'), filesize(__DIR__.'/../includes/persondata.sql'));
        foreach ($items as $item) {
            $itemid= str_replace('http://www.wikidata.org/entity/', 'wd:', $item['item']['value']);
            $querytxt=str_replace('$1',$itemid,$this->querytxt);
            $this->parsePersonData($queryhandler->fetchData($querytxt));
        }
    }
    
    /**
     * 
     * @param array $personData
     */
    function parsePersonData($personData) {
        var_dump($personData[0]); die();
    }
}
