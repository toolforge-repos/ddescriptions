SELECT ?jobLabel ?birth ?death ?jobFemale ?country ?countryLabel ?sex WHERE {
    VALUES ?item { $1 }
    ?item wdt:P106 ?job; wdt:P21 ?sex; wdt:P27 ?country .
    OPTIONAL { ?item p:P569/psv:P569 [
                wikibase:timePrecision ?birthPrec ; # precision of at least day
                wikibase:timeValue ?birth ;
              ] FILTER(?birthPrec >= 9)}
    OPTIONAL { ?item p:P570/psv:P570 [
                wikibase:timePrecision ?deadPrec ; # precision of at least day
                wikibase:timeValue ?dead ;
              ] FILTER(?deadPrec >= 9) }
    OPTIONAL { ?job wdt:P2521 ?jobFemale . FILTER (LANG(?jobFemale)="nl") }
    SERVICE wikibase:label { bd:serviceParam wikibase:language "nl". }
}
