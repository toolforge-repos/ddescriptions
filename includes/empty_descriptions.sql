SELECT ?item WHERE {
    ?item wdt:P31 wd:Q5.
    OPTIONAL {
        ?item schema:description ?itemDescription.
        FILTER((LANG(?itemDescription)) = "nl")
    }
} LIMIT 100
